package org.abbtech.practice.service;

import org.abbtech.practice.model.Notification;

import java.awt.print.Book;
import java.util.List;

public interface NotificationService {
   Notification sendNotification(Notification notification);
}
