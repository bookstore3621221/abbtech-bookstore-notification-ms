package org.abbtech.practice.service.impl;

import lombok.RequiredArgsConstructor;
import org.abbtech.practice.model.Notification;
import org.abbtech.practice.repository.NotificationRepository;
import org.abbtech.practice.service.NotificationService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NotificationServiceImpl implements NotificationService {
    private final NotificationRepository notificationRepository;

    @Override
    public Notification sendNotification(Notification notification) {
        //TODO  Consume in notification service to send notification
        return notificationRepository.save(notification);
    }
}
