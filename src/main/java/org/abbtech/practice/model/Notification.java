package org.abbtech.practice.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Entity
@Table(name = "notification", schema = "book_delivery")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Notification {
    @Id
    @GeneratedValue(generator = "UUID")
    @Column(name = "id", nullable = false)
    private UUID id;

    @Column(name = "message",nullable = false)
    private String message;

    @Column(name = "user_email",nullable = false)
    private String userEmail;

    @Column(name = "user_id",nullable = false)
    private UUID userId;

    @Column(name = "order_id",nullable = false)
    private UUID orderId;
}
