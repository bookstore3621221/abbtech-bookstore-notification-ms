package org.abbtech.practice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.abbtech.practice.model.Notification;
import org.abbtech.practice.service.NotificationService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/notifications")
@RequiredArgsConstructor
@Tag(name = "Notification Controller")
public class NotificationController {
    private final NotificationService notificationService;

    @PostMapping
    @Operation(summary = "Send a notification")
    public Notification sendNotification(@RequestHeader(value = "X-USER-ID", required = false) String userId,
                                         @RequestHeader(value = "X-USER-EMAIL", required = false) String userEmail,
                                         @RequestBody Notification notification) {
        return notificationService.sendNotification(notification);
    }
}
