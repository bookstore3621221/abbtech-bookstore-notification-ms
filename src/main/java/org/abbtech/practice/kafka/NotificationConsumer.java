package org.abbtech.practice.kafka;

import org.abbtech.practice.dto.KafkaDto;
import org.abbtech.practice.model.Notification;
import org.abbtech.practice.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class NotificationConsumer {

    @Autowired
    private NotificationRepository notificationRepository;

    @KafkaListener(topics = "orderConfirmed", groupId = "notification_group")
    public void handleOrderConfirmed(KafkaDto kafkaDto) {
        Notification notification = new Notification();
        notification.setOrderId(UUID.fromString(kafkaDto.getOrderId()));
        notification.setUserId(UUID.fromString("75ae946f-4850-4734-8d45-1620d0d3bf21"));
        notification.setUserEmail(kafkaDto.getUserEmail());
        notification.setMessage("Your order has been confirmed!");
        notificationRepository.save(notification);
    }
}
